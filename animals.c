///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   04_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

   // @todo Map the enum Color to a string
   switch(color) {

      case 0:
         return "Black";
         break;

      case 1:
         return "White";
         break;

      case 2:
         return "Red";
         break;

      case 3:
         return "Blue";
         break;

      case 4:
         return "Green";
         break;

      case 5:
         return "Pink";
         break;

      default:
         return NULL;
         break;
   }
}

/// Decode the enum Gender into strings for printf()

char* genderName (enum Gender gender) {

      switch(gender) {

      case 0:
         return "Male";
         break;

      case 1:
         return "Female";
         break;

      default:
         return NULL;
         break;
   }
}

/// Decode the enum Gender into strings for printf()

char* breedName  (enum CatBreeds breeds) {

   switch(breeds) {

      case 0:
         return "Main Coon";
         break;

      case 1:
         return "Manx";
         break;

      case 2:
         return "Shorthair";
         break;

      case 3:
         return "Persian";
         break;

      case 4:
         return "Sphinx";
         break;

      default:
         return NULL;
         break;
   }
}

// If fixed return yes or return no if not fixed
char* animalFixed (bool isFixed) {

   if(true) 
      return "yes";

   else
      return "no";
}

//   return NULL; // We should never get here
// };


